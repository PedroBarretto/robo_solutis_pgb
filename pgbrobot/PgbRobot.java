package pgbrobo;
import robocode.*;
import robocode.AdvancedRobot;
import java.awt.*;
import robocode.util.Utils;
import java.util.Arrays;

public class PgbRobot extends AdvancedRobot{
	int direction = 1;

	
	public void run() {
		setBodyColor(new Color(0, 0, 0));
        setGunColor(new Color(94, 94, 94));
        setRadarColor(new Color(255, 255, 255));
        setScanColor(new Color(145, 145, 145));
        setBulletColor(new Color(255, 255, 255));
        
   
		
		while(true) {
			
		}
	}

	public void onScannedRobot(ScannedRobotEvent e) {
		
		if (e.getBearing() >= 0) {
			direction = 1;
		} else {
			direction = -1;
		}
		
		turnRight(e.getBearing());
		ahead(e.getDistance());
		scan();
	}
	

	public void onHitByBullet(HitByBulletEvent e) {
		if (getEnergy() < 60) {
			turnRight(90 - e.getBearing());
		}
		
	}
	
	public void onHitRobot(HitRobotEvent e) {
		//double turnGunAmt = Utils.normalRelativeAngleDegrees(e.getBearing() + getHeading() - getGunHeading());
		
		if (e.getBearing() >= 0) {
			direction = 1;
		} else {
			direction = -1;
		}
		
		turnRight(e.getBearing());
		
		if (getEnergy() > 50) {
			fire(5);
		} else {
			fire(3);
		}
		
		ahead(40);
	}
	
	public void onHitWall(HitWallEvent e) {
		double wallAngle = e.getBearing();
		turnRight(-wallAngle);
		ahead(150);
	}	
	
	public void onWin(WinEvent e) {
		for (int i = 0; i < 50; i++) {
			turnRight(30);
			turnLeft(30);
		}
	}

}
