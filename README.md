# Robô para Robot Arena Solutis 2020

Meu nome é Pedro Galvão Barretto, tenho 19 anos, e este é o robô que eu fiz para o Talent Sprint Robot Arena da Solutis.

O objetivo deste Robô é chegar o mais perto possível do inimigo, para começar a disparar projéteis de alto dano.

Esse foi meu primeiro contato com Java, então busquei aproveitar ao máximo para aprender os recursos que a linguagem tem para me oferecer.

Sobre o código:

- No começo do código, temos a definição das cores do robô, algo que com certeza é bastante importante, afinal de contas, o que seria um jogo sem customização!

- Na parte que escaneamos outros robôs, 'onScannedRobot', o pgbrobot decide se vai para o sentido horário ou anti-horário, isso para a rotação não ser para o lado contrário do robô inimigo escaneado. Após decidir a direção, o pgbrobot anda em sentido ao inimigo.

- Se quando o pgbrobot estiver indo em direção ao inimigo, estiver com pouca vida, e receber um tiro, no evento 'onHitByBullet', ele faz uma curva para tentar desviar de possíveis outros tiros.

- Agora chegamos na parte mais interressante do robô. O sistema de atirar do pgbrobot está dentro do evento de colisão com outro robô, 'onHitRobot'. Isso porque esse foi a maneira que encontrei de acertar mais tiros, e causar mais dano nos meus inimigos.

- Quando uma colisão de robôs acontece, o pgbrobot se vira em direção ao inimigo e começa a disparar tiros de alto dano.

- Essa mecânica implementada é uma "faca de dois gumes", pois tem suas vantagens e desvantagens. A desvantagem seria que o inimigo acerta tiros com mais facilidade, porém, a vantagem, é que além de o pgbrobot acertar tiros quase que certeiramente, como o tiro acertado é, geralmente, de um dano maior que o do inimigo, matamos mais rápido o inimigo do que o inimigo mata o pgbrobot. E ainda recuperamos vida por tiro acertado, ou seja, a depender da situação, podemos sair com mais vida do que entramos em batalha.

- Para finalizar, quando o pgbrobot ganha uma batalha, ele faz uma dança da vitória. Espero que possa fazer essa dança na Robot Arena!